# Initial set up for a container!
# This scirpt was developed to make installing everything I need
# on a new debian-based system automatic

# Make sure we are in the home directory
cd ~

# Install wget, this is needed to automate downloads

sudo apt-get install wget -y

# Install git
sudo apt-get install git -y
git config --global user.email "bauerjoseph13@gmail.com"
git config --global user.name "Joseph Bauer"

# Get my .vimrc
git init
git remote add origin https://github.com/bauerjoseph/dotfiles.git
git pull origin master

#Install Pathogen, a vim package manager
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

# Install vim airline, a status bar for vim
git clone https://github.com/vim-airline/vim-airline ~/.vim/bundle/vim-airline

# Install VS Code
sudo apt-get install gpg -y
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt-get update
sudo apt-get install code -y

# Install Discord
wget "https://discordapp.com/api/download?platform=linux&format=deb" -O discord.deb
sudo dpkg -i discord.deb
sudo apt-get install -f -y
rm discord.deb

# Install node and npm 
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

# Node packages that I use
sudo npm install nodemon -g
sudo npm install express-generator -g

# Docker
sudo apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-commonA

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
sudo apt-get update 
sudo apt-get install docker-ce -y
sudo apt-get install docker-compose -y
